import React, {useState} from "react";
import {connect} from "react-redux";

import {deleteTodo, updateTodo} from "./todoSlice";


const Todo = ({checked, name, id, updateTodo, removeTodo}) => {
  /** Render Todo instance. */

  const [text, setText] = useState(name);
  const [done, setDone] = useState(checked);

  const handleDone = () => {
    updateTodo({id: id, name: text, checked: !done});
    setDone(!done);
  };

  const handleUpdate = (e) => {
    e.preventDefault();
    document.activeElement.blur();
    updateTodo({id: id, name: text, checked: done});
  };

  return (
    <li>
      <form onSubmit={handleUpdate}>
        <input type="checkbox" checked={done} onChange={handleDone}/>
        <input type="text" spellCheck="false" value={text} onChange={e => setText(e.target.value)} onBlur={handleUpdate}
               className="app-input" style={{textDecoration: done ? 'line-through' : 'none'}}/>
        <img src={require("../../images/delete.svg")} alt={"remove-todo"} onClick={() => removeTodo({id: id})}/>
      </form>
    </li>
  )
};


const mapDispatchToProps = dispatch => {
  return {
    updateTodo: (data) => dispatch(updateTodo(data)),
    removeTodo: (data) => dispatch(deleteTodo(data))
  }
};

export default connect(null, mapDispatchToProps)(Todo);