import React, {useState} from "react";
import {connect} from "react-redux";

import {createTodo} from "./todoSlice";
import "./AddTodo.scss"


const AddTodo = ({addTodo}) => {
  /** Render button and create new instance when clicked. */

  const [name, setName] = useState("");

  const handleCreate = e => {
    e.preventDefault();
    if (name.length) {
      addTodo({name: name});
      setName('');
    }
  };

  return (
    <div className="app-add">
      <form onSubmit={e => handleCreate(e)}>
        <input placeholder="What to do?" spellCheck="false" value={name} onChange={e => setName(e.target.value)}
               className="app-input"/>
        <input type="image" alt="Add todo" src={require("../../images/add.svg")}/>
      </form>
    </div>
  )
};

const mapDispatchToProps = dispatch => {
  return {
    addTodo: (data) => dispatch(createTodo(data)),
  }
};

export default connect(null, mapDispatchToProps)(AddTodo);