import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";

import {apiGetAll, apiCreate, apiUpdate, apiDelete} from "../../apiClient";


/*
  Middleware responsible for API interactions.
*/
export const getAllTodos = createAsyncThunk(
  'todos/getTodosStatus',
  async () => {
    const response = await apiGetAll();
    return response.data;
  }
);

export const deleteTodo = createAsyncThunk(
  'todos/deleteTodoStatus',
  async (data) => {
    await apiDelete(data);
    return data;
  }
);

export const createTodo = createAsyncThunk(
  'todos/addTodoStatus',
  async (data) => {
    const response = await apiCreate(data);
    return response.data;
  }
);

export const updateTodo = createAsyncThunk(
  'todos/updateTodoStatus',
  async (data) => {
    const response = await apiUpdate(data);
    return response.data;
  }
);

/*
  Main slice. Reduces state after given thunk receives response from API.
*/
const todoSlice = createSlice({
  name: 'todos',
  initialState: {instances: []},
  extraReducers: {
    [getAllTodos.fulfilled]: (state, action) => {
      state.instances = [...action.payload];
    },
    [createTodo.fulfilled]: (state, action) => {
      state.instances.push(action.payload);
    },
    [updateTodo.fulfilled]: (state, action) => {
      state.instances.forEach((todo, index) => {
        if (todo.id === action.payload.id) {
          state[index] = action.payload
        }
      });
    },
    [deleteTodo.fulfilled]: (state, action) => {
      state.instances = state.instances.filter(todo => todo.id !== action.payload.id)
    },
  }
});


export const getLocalTodos = state => state.instances;
export default todoSlice.reducer;