import React, {useEffect} from "react";
import {connect} from "react-redux";

import Todo from "./Todo";
import {getAllTodos, getLocalTodos} from "./todoSlice"
import "./TodoList.scss"


const TodoList = ({todos, initializeTodoList}) => {
  /** Render list of Todos. */

  useEffect(() => {
    if (!todos.length) {
      initializeTodoList();
    }
  }, [todos, initializeTodoList]);

  return (
    <div className="app-list">
      <ul>
        {todos.map(todo => <Todo key={todo.id} id={todo.id} checked={todo.checked} name={todo.name}/>)}
      </ul>
    </div>
  )
};


const mapStateToProps = state => {
  return {
    todos: getLocalTodos(state.todos),
  }
};

const mapDispatchToProps = dispatch => {
  return {
    initializeTodoList: () => dispatch(getAllTodos()),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);