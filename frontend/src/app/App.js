import React from 'react';

import AddTodo from '../features/todos/AddTodo';
import TodoList from "../features/todos/TodoList";

import './App.scss';


function App() {
  return (
    <div>
      <h1 className="app-title">Roland's Todos</h1>
      <AddTodo/>
      <TodoList/>
    </div>
  );
}

export default App;
