import axios from "axios";


const production = 'https://rolstodos.herokuapp.com/api/';
const development = 'http://127.0.0.1:8000/api/';

// Custom axios client with predefined baseURL.
const apiClient = axios.create({
    baseURL: process.env.NODE_ENV === "development" ? development : production
  }
);


/*
  Basic functions to connect to api, which perform get/post/patch/delete api calls.
*/

export async function apiGetAll() {
  try {
    return await apiClient.get(`todos/`);
  } catch (error) {
    console.log(error);
  }
}

export async function apiCreate(data) {
  try {
    return await apiClient.post(`todos/`, data);
  } catch (error) {
    console.log(error);
  }
}

export async function apiUpdate(data) {
  try {
    return await apiClient.patch(`todos/${data.id}/`, data);
  } catch (error) {
    console.log(error);
  }
}

export async function apiDelete(data) {
  try {
    return await apiClient.delete(`todos/${data.id}/`);
  } catch (error) {
    console.log(error);
  }
}

