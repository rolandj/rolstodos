from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import AllowAny

from .models import Todo
from .serializers import TodoSerializer


class TodoViewSet(ModelViewSet):
    serializer_class = TodoSerializer
    permission_classes = (AllowAny,)

    def get_queryset(self):
        return Todo.objects.all().order_by('checked', '-updated_at')
