from rest_framework.serializers import ModelSerializer

from .models import Todo


class TodoSerializer(ModelSerializer):
    class Meta:
        model = Todo
        fields = ('id', 'name', 'checked', 'created_at', 'updated_at')
        read_only_fields = ('created_at', 'updated_at')
