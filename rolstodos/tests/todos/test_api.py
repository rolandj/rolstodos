from unittest.mock import patch
from datetime import datetime

from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework.reverse import reverse

from rolstodos.tests.factories import TodoFactory


class TestApi(TestCase):
    """Test API calls on basic CRUD methods.

    Performs api calls with mock assertion as for POST request and checks status codes for RUD methods."""

    def setUp(self) -> None:
        self.client = APIClient()
        self.data = dict(name='Lorem ipsum, I have work to do.')
        self.base_url = reverse('todo-list')
        self.instances = [TodoFactory() for i in range(15)]
        self.mock_data = dict(name=self.data['name'], checked=False, created_at=datetime.now(),
                              updated_at=datetime.now())

    def test_creating_todo(self):
        with patch('rolstodos.apps.todos.views.Todo.objects.create') as create_mock:
            create_mock.return_value = self.mock_data
            response = self.client.post(self.base_url, self.data)

            create_mock.assert_called_once_with(name=self.data['name'], checked=False)
            self.assertEqual(response.status_code, 201)

    def test_cant_create_todo_with_empty_name(self):
        self.data['name'] = ''
        response = self.client.post(self.base_url, self.data)

        self.assertEqual(response.status_code, 400)

    def test_patching_todos_name(self):
        self.data['name'] = 'Lorem ipsum I must update name'
        response = self.client.patch(f'{self.base_url}1/', self.data)

        self.assertEqual(response.status_code, 200)

    def test_patching_todos_checked(self):
        self.data['checked'] = 'true'
        response = self.client.patch(f'{self.base_url}1/', self.data)

        self.assertEqual(response.status_code, 200)

        self.data['checked'] = 'false'
        response = self.client.patch(f'{self.base_url}1/', self.data)

        self.assertEqual(response.status_code, 200)

    def test_updating_todos_name_and_checked(self):
        self.data['name'] = 'Lorem ipsum I must update name'
        self.data['checked'] = 'false'
        response = self.client.put(f'{self.base_url}1/', self.data)

        self.assertEqual(response.status_code, 200)

        self.data['name'] = 'Lorem ipsum I must update name again'
        self.data['checked'] = 'true'
        response = self.client.put(f'{self.base_url}1/', self.data)

        self.assertEqual(response.status_code, 200)

    def test_cant_update_todo_with_empty_name(self):
        self.data['name'] = ''
        response = self.client.patch(f'{self.base_url}1/', self.data)

        self.assertEqual(response.status_code, 400)

    def test_deleting_todo(self):
        response = self.client.delete(f'{self.base_url}1/')
        self.assertEqual(response.status_code, 204)

    def test_getting_todo(self):
        response = self.client.get(f'{self.base_url}1/')
        self.assertEqual(response.status_code, 200)

    def test_getting_all_todos(self):
        response = self.client.get(f'{self.base_url}')
        self.assertEqual(response.status_code, 200)
