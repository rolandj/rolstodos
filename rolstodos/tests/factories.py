import factory

from rolstodos.apps.todos.models import Todo


class TodoFactory(factory.DjangoModelFactory):
    class Meta:
        model = Todo

    name = factory.Faker('word')
    created_at = factory.Faker('date')
    updated_at = factory.Faker('date')
    checked = factory.Faker('boolean')
