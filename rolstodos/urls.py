"""rolstodos URL Configuration"""

from django.contrib import admin
from django.urls import path, include, re_path

from rolstodos.apps.todos.urls import urls as todos_urls
from rolstodos.apps.frontend.views import index

urlpatterns = [
    path('api/admin/', admin.site.urls),
    path('api/todos/', include(todos_urls)),
    re_path('.*', index)
]
